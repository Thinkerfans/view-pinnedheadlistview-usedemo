package com.way.pinnedheaderlistview;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import com.android.common.view.FilterView;
import com.android.common.view.PinnedHeaderListView;
import com.android.common.view.TwoValue;

public class MainActivity extends Activity implements OnItemClickListener{
	private static final String FORMAT = "^[a-z,A-Z].*$";
	private PinnedHeaderListView mListView;

	private FriendsListViewAdapter mAdapter;
	private ArrayList<FriendMode> mList;
	private FilterView mFilterView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initData();
		initView();
	}

	private void initData() {
		String[] datas = getResources().getStringArray(R.array.countries);
		HashSet<String> headerList = new LinkedHashSet<String>();

		mList = new ArrayList<FriendMode>();
		int belongTo = 0;
		for (int i = 0; i < datas.length; i++) {
			String firstName = datas[i].substring(0, 1);

			if (!headerList.contains(firstName)) {
				belongTo = mList.size();
				FriendMode head = new FriendMode();
				head.setName(firstName);
				head.setItemType(FriendMode.TYPE_PARENT);
				head.setBelongTo(belongTo);
				
				mList.add(head);
				headerList.add(firstName);
				System.out.println(head);
			}

			FriendMode mode = new FriendMode();
			mode.setName(datas[i]);
			mode.setItemType(FriendMode.TYPE_CHILD);
			mode.setBelongTo(belongTo);
			mList.add(mode);

		}
	}
	private void initView() {
		// TODO Auto-generated method stub
		mListView = (PinnedHeaderListView) findViewById(R.id.list_view);
		mAdapter = new FriendsListViewAdapter(this);
		mAdapter.setList(mList);
		mListView.setAdapter(mAdapter);
		mListView.setPinnedHeaderView(LayoutInflater.from(this).inflate(
				R.layout.pinnedheader_listview_head, mListView, false));
		mListView.setOnItemClickListener(this);
		mListView.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				
				if(mFilterView != null){
					mFilterView.setCurrentListIndex(firstVisibleItem);
				}
				
				if (view.getVisibility() == View.GONE) {
					return;
				}
				if (view instanceof PinnedHeaderListView) {
					((PinnedHeaderListView) view)
							.configureHeaderView(firstVisibleItem);
				}		
			}

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub		
			}
		});
		
		mFilterView = (FilterView) findViewById(R.id.list_filter);
		List<TwoValue<String, Integer>> filterArray = new ArrayList<TwoValue<String,Integer>>();
		for(FriendMode cr : mList){
			if (cr.getItemType() == FriendMode.TYPE_CHILD) {
				String py = cr.getName();
				if(!TextUtils.isEmpty(py)){
					String first  = py.substring(0, 1);
					TwoValue<String, Integer> tv = new TwoValue<String, Integer>(first.toUpperCase(), cr.getBelongTo());
					if(!filterArray.contains(tv)){
						filterArray.add(tv);
					}				
				}
            }
		}
		mFilterView.setArray(filterArray);
		mFilterView.setListView(mListView);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		System.out.println(arg2);
		Toast.makeText(MainActivity.this,
				" name is " + mList.get(arg2).getName(),
				Toast.LENGTH_SHORT).show();		
	}

}
