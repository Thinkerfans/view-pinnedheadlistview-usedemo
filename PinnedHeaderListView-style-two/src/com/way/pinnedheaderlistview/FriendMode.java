package com.way.pinnedheaderlistview;



public class FriendMode {

	public static final int TYPE_PARENT = 1;
	public static final int TYPE_CHILD = 0;

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	protected int belongTo;
	protected int itemType;

	public int getItemType() {
		return itemType;
	}

	public void setItemType(int type) {
		this.itemType = type;
	}

	public int getBelongTo() {
		return belongTo;
	}

	public void setBelongTo(int belongTo) {
		this.belongTo = belongTo;
	}

	@Override
	public String toString() {
		return "FriendMode [name=" + name + ", belongTo=" + belongTo
				+ ", itemType=" + itemType + "]";
	}
}
