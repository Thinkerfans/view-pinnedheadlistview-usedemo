package com.android.common.view;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class FilterView extends LinearLayout {
//	private static final String TAG = "FilterView";
	private List<TwoValue<String, Integer>> mArray;
	private ListView mListView;
	public FilterView(Context context) {
		super(context);
		init();
	}

	public FilterView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	@SuppressLint("NewApi")
	public FilterView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init(){
		setOrientation(VERTICAL);
	}

	public void setArray(List<TwoValue<String, Integer>> array){
		removeAllViews();
		mArray = array;
		setWeightSum(array.size());

		for (TwoValue<String, Integer> value : array) {
			TextView tv = createTextView();
			tv.setText(value.first);
			addView(tv);
		}
	}

	public void setListView(ListView listview){
		mListView = listview;
	}

	public void setCurrentListIndex(int index){
		if (mArray == null) {
			return;
		}
		clearColor();
		int selected = 0;
		for(int i = 0; i < mArray.size(); i++){
			TwoValue<String, Integer> value = mArray.get(i);
			if (index < value.second) {
				selected = i - 1;
				break;
			}
		}
		if (selected >= 0 && selected < getChildCount()) {
			View v = getChildAt(selected);
			if(v instanceof TextView){
				TextView tv = (TextView) v;
				tv.setTextColor(0xff12b0f0);
			}
		}
	}

	private TextView createTextView(){
		TextView tv = new TextView(getContext());
		tv.setClickable(true);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0, 1);
		tv.setLayoutParams(lp);
		tv.setGravity(Gravity.CENTER);

		tv.setSingleLine(true);
		tv.setHorizontallyScrolling(false);
		tv.setTextColor(Color.BLACK);
		tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
		return tv;
	}

	private void clearColor(){
		for(int i = 0; i < getChildCount(); i++){
			View v = getChildAt(i);
			if(v instanceof TextView){
				TextView tv = (TextView) v;
				tv.setTextColor(Color.BLACK);
			}
		}
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		if (mArray == null) {
	        return super.onInterceptTouchEvent(ev);
        }
		if (ev.getAction() == MotionEvent.ACTION_MOVE || ev.getAction() == MotionEvent.ACTION_UP) {
			int height = getHeight() / mArray.size();
			int index = (int) (ev.getY() / height);
			clearColor();
			if (index >= 0 && index < getChildCount()) {
				getChildAt(index).setSelected(true);

				if (mListView != null) {
					mListView.setSelection(mArray.get(index).second);
				}
			}
		} else {
			clearColor();
		}
		return super.onInterceptTouchEvent(ev);
	}
}
