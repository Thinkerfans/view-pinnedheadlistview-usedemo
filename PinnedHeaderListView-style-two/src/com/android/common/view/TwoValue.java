package com.android.common.view;

public class TwoValue<F, S> {
    public final F first;
    public final S second;

    public TwoValue(F first, S second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof TwoValue)) {
            return false;
        }
        TwoValue<?, ?> p = (TwoValue<?, ?>) o;

        return safeEqual(p.first, first) && safeEqual(p.second, second);
    }

    @Override
    public int hashCode() {
        return (first == null ? 0 : first.hashCode()) ^ (second == null ? 0 : second.hashCode());
    }

    public static <A, B> TwoValue <A, B> create(A a, B b) {
        return new TwoValue<A, B>(a, b);
    }
    
    private boolean safeEqual(Object a, Object b){
        return a == b || (a != null && a.equals(b));
    }
}