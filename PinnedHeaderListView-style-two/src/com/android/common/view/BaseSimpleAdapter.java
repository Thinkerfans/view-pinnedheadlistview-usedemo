package com.android.common.view;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class BaseSimpleAdapter<T> extends BaseAdapter {

	protected Context mContext;
	protected List<T> mList;
	private LayoutInflater mInflater;

	public BaseSimpleAdapter(Context context) {
		init(context);
	}

	public List<T> getList() {
		return mList;
	}

	public void setList(List<T> mList) {
		this.mList = mList;
	}
	
	public BaseSimpleAdapter(Context context, List<T> list) {
		mList = list;
		init(context);
	}
	
	public LayoutInflater getInflater() {
		return mInflater;
	}

	private void init(Context context) {
		mContext = context;
		mInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return null == mList ? 0 : mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null==mList ? null : mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public abstract View getView(int position, View convertView,
			ViewGroup parent);

}
