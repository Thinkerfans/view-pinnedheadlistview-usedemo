package com.way.pinnedheaderlistview;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.TextView;

import com.user.view.BaseSimpleAdapter;
import com.user.view.PinnedHeaderListView;
import com.user.view.PinnedHeaderListView.PinnedHeaderAdapter;

public class FriendsListViewAdapter extends BaseSimpleAdapter<FriendMode>
		implements PinnedHeaderAdapter ,OnScrollListener{

	public FriendsListViewAdapter(Context context) {
		super(context);
	}

	@Override
	public boolean areAllItemsEnabled() {
		return true;
	}

	@Override
	public boolean isEnabled(int position) {
		if (mList.get(position).getItemType() == FriendMode.TYPE_PARENT) {
			return false;
		}
		return true;
	}

	@Override
	public int getItemViewType(int position) {
		return mList.get(position).getItemType();
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		FriendMode item = mList.get(position);

		if (item.getItemType() == FriendMode.TYPE_PARENT) {
			ParentHolderView holder;
			if (convertView == null) {
				holder = new ParentHolderView();
				convertView = getInflater().inflate(
						R.layout.pinnedheader_listview_head, null);
				holder.parentTv = (TextView) convertView
						.findViewById(R.id.friends_list_header_text);
				convertView.setTag(holder);
			} else {
				holder = (ParentHolderView) convertView.getTag();
			}

			holder.parentTv.setText(item.getName());

		} else {
			ChildHolderView holder;
			if (convertView == null) {
				holder = new ChildHolderView();
				convertView = getInflater().inflate(
						R.layout.pinnedheader_listview_item, null);
				holder.childTv = (TextView) convertView
						.findViewById(R.id.friends_item);
				convertView.setTag(holder);
			} else {
				holder = (ChildHolderView) convertView.getTag();
			}

			holder.childTv.setText(item.getName());

		}
		return convertView;
	}

	@Override
	public int getPinnedHeaderState(int position) {	
		if (position < 0) {
			return PINNED_HEADER_GONE;
		}

		if (position + 1 < mList.size()) {
			int type = mList.get(position + 1).getItemType();
			if (type == FriendMode.TYPE_PARENT && position != 0) {
				return PINNED_HEADER_PUSHED_UP;
			}
		}
		return PINNED_HEADER_VISIBLE;
	}

	@Override
	public void configurePinnedHeader(View header, int position, int alpha) {
		
		int belongTo = mList.get(position+1).getBelongTo();
		FriendMode item = mList.get(belongTo);
		((TextView) header.findViewById(R.id.friends_list_header_text))
				.setText(item.getName());
	}

	private static class ParentHolderView {
		TextView parentTv;
	}

	private static class ChildHolderView {
		TextView childTv;
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		if (view.getVisibility() == View.GONE) {
			return;
		}
		if (view instanceof PinnedHeaderListView) {
			((PinnedHeaderListView) view)
					.configureHeaderView(firstVisibleItem);
		}		
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub		
	}

}
